variable "db_clusters" {
  type = map(object({
    name       = string
    type       = string
    hash_type  = optional(string)
    project_id = optional(number)

    preset = optional(object({
      location = string
      price = optional(object({
        min = number
        max = number
      }))
      disk_size = number
    }))
    users = list(object({
      username        = string
      password_length = number
      description     = optional(string)
    }))
    databases = list(object({
      name        = list(string)
      description = optional(string)
    }))
    parameters = optional(object({
      auto_increment_increment            = optional(string)
      auto_increment_offset               = optional(string)
      innodb_io_capacity                  = optional(string)
      innodb_purge_threads                = optional(string)
      innodb_read_io_threads              = optional(string)
      innodb_thread_concurrency           = optional(string)
      innodb_write_io_threads             = optional(string)
      join_buffer_size                    = optional(string)
      max_allowed_packet                  = optional(string)
      max_heap_table_size                 = optional(string)
      autovacuum_analyze_scale_factor     = optional(string)
      bgwriter_delay                      = optional(string)
      bgwriter_lru_maxpages               = optional(string)
      deadlock_timeout                    = optional(string)
      gin_pending_list_limit              = optional(string)
      idle_in_transaction_session_timeout = optional(string)
      idle_session_timeout                = optional(string)
      join_collapse_limit                 = optional(string)
      lock_timeout                        = optional(string)
      max_prepared_transactions           = optional(string)
      max_connections                     = optional(string)
      shared_buffers                      = optional(string)
      wal_buffers                         = optional(string)
      temp_buffers                        = optional(string)
      work_mem                            = optional(string)
      sql_mode                            = optional(string)
      query_cache_type                    = optional(string)
      query_cache_size                    = optional(string)
    }))
  }))
}

variable "timeweb_token" {
  description = "TimeWebCloud Token"
  type        = string
  nullable    = true
  default     = null
}