module "main_db" {
  for_each = var.db_clusters

  source = "../"

  name       = each.value.name
  type       = each.value.type
  hash_type  = each.value.hash_type
  project_id = each.value.project_id

  preset = each.value.preset

  parameters = each.value.parameters
  users      = each.value.users
  databases  = each.value.databases

  providers = {
    twc = twc.default
  }
}

provider "twc" {
  token = var.timeweb_token
  alias = "default"
}
