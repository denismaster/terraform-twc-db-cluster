variable "timeweb_token" {
  description = "TimeWebCloud Token"
  type        = string
  nullable    = true
  default     = null
}

variable "name" {
  description = "Name for database cluster"
  type        = string
}

variable "project_id" {
  description = "Project ID for managed resource"
  type        = string
  nullable    = true
  default     = null
}

variable "type" {
  description = "Type of database cluster"
  type        = string

  validation {
    condition     = contains(["mysql", "mysql5", "postgres", "postgres14", "postgres15", "redis", "mongodb", "opensearch", "kafka", "rabbitmq"], var.type)
    error_message = "Error! Database type does not support"
  }
}

variable "hash_type" {
  description = "Hash type for database"
  type        = string
  nullable    = true
  default     = null

  validation {
    condition     = contains(["mysql_native", "caching_sha2"], coalesce(var.hash_type, "mysql_native"))
    error_message = "Error! Hash type does not support"
  }
}

variable "parameters" {
  description = "Configuration parameters for database cluster"
  type = object({
    auto_increment_increment            = optional(string)
    auto_increment_offset               = optional(string)
    innodb_io_capacity                  = optional(string)
    innodb_purge_threads                = optional(string)
    innodb_read_io_threads              = optional(string)
    innodb_thread_concurrency           = optional(string)
    innodb_write_io_threads             = optional(string)
    join_buffer_size                    = optional(string)
    max_allowed_packet                  = optional(string)
    max_heap_table_size                 = optional(string)
    autovacuum_analyze_scale_factor     = optional(string)
    bgwriter_delay                      = optional(string)
    bgwriter_lru_maxpages               = optional(string)
    deadlock_timeout                    = optional(string)
    gin_pending_list_limit              = optional(string)
    idle_in_transaction_session_timeout = optional(string)
    idle_session_timeout                = optional(string)
    join_collapse_limit                 = optional(string)
    lock_timeout                        = optional(string)
    max_prepared_transactions           = optional(string)
    max_connections                     = optional(string)
    shared_buffers                      = optional(string)
    wal_buffers                         = optional(string)
    temp_buffers                        = optional(string)
    work_mem                            = optional(string)
    sql_mode                            = optional(string)
    query_cache_type                    = optional(string)
    query_cache_size                    = optional(string)
  })
  nullable = false
  default  = {}
}

variable "preset" {
  description = "Settings for the database with preset"
  type = object({
    location  = string
    disk_size = number
    price = optional(object({
      min = number
      max = number
    }))
  })
}

variable "users" {
  description = "Data of users"
  type = list(object({
    username        = string
    password_length = number
    description     = optional(string)
  }))
}

variable "databases" {
  description = "Data of databases"
  type = list(object({
    name        = list(string)
    description = optional(string)
  }))
}