# terraform-twc-db-cluster

## Example

```terraform
module "main_db" {
  source        = "./modules/db_cluster"

  name       = "db-postgres"
  type       = "postgres"
  project_id = twc_project.project.id
  hash_type  = "mysql_native"

  preset = {
    location = "ru-1"
    price = {
      min = 100
      max = 900
    }
    disk_size = 20 * 1024
  }

  parameters = {
      auto_increment_increment  = "100"
      auto_increment_offset     = "1"
      innodb_io_capacity        = "200"
      innodb_purge_threads      = "4"
      innodb_read_io_threads    = "4"
      innodb_thread_concurrency = "0"
      innodb_write_io_threads   = "4"
  }

  users = [
    {
      username        = "postgres1"
      password_length = 25
    },
    {
      username        = "postgres2"
      password_length = 15
      description     = "User for dev enviromant"
    }
  ]

  databases = [
    {
      name        = ["example_prod1", "example_prod2", "example_prod3"]
      description = "for prod"
    },
    {
      name        = ["example_dev1", "example_dev2"]
      description = "for dev"
    }
  ]

  providers = {
    twc = twc.default
  }
}

provider "twc" {
  token = var.timeweb_token
  alias = "default"
}
```