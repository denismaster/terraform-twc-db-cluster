output "host" {
  description = "Return database IP"
  value       = twc_database_cluster.db_cluster.networks.1.ips.0.ip
}

output "port" {
  description = "Return database port"
  value       = twc_database_cluster.db_cluster.port
}

output "users" {
  description = "Return users"
  value = [
    for user in local.users : {
      username    = user.username
      password    = user.password
      description = user.description
    }
  ]
  sensitive = true
}